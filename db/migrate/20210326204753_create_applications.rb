class CreateApplications < ActiveRecord::Migration[6.1]
  def change
    create_table :applications do |t|
      t.integer :post_id
      t.integer :user_id
      t.string :status, default: 'not_seen', null: false
      t.timestamps
    end
  end
end
