class CreateUsersJoinTablePosts < ActiveRecord::Migration[6.1]
  def change
    add_reference :users, :posts, foreign_key: true
  end
end
