# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/applications routes', type: :routing do
  it 'routes to applications#index' do
    expect(get('/applications')).to route_to('applications#index')
  end

  it 'routes to applications#show' do
    expect(get('/applications/1')).to route_to('applications#show', id: '1')
  end

  it 'routes to applications#create' do
    expect(post('/applications')).to route_to('applications#create')
  end

  it 'routes to applications#update' do
    expect(put('/applications/1')).to route_to('applications#update', id: '1')
    expect(patch('/applications/1')).to route_to('applications#update', id: '1')
  end

  it 'routes to applications#destroy' do
    expect(delete('/applications/1')).to route_to('applications#destroy', id: '1')
  end
end
