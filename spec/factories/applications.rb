FactoryBot.define do
  factory :application do
    association :user
    association :post
    status { 'not_seen' }
  end
end
