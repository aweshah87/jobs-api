FactoryBot.define do
  factory :post do
    sequence(:title) { |n| "Sample job post #{n}" }
    sequence(:description) { |n| "Sample description #{n}" }
    expiry_date { 1.day.after }
    created_at { Time.now }
    association :user
  end
end
