require 'rails_helper'

RSpec.describe Application, type: :model do
  describe '#validations' do
    let(:application) { build(:application) }

    it 'tests that factory is valid' do
      expect(application).to be_valid
      application.save!
      another_application = build(:application)
      expect(another_application).to be_valid
    end

    it 'has an invalid post_id' do
      application.post_id = 0
      expect(application).to be_invalid
    end
  end

  describe '.not_seen' do
    it 'returns not_seen applications' do
      not_seen_app = create(:application)
      expect(described_class.not_seen).to eq( [not_seen_app] )
      not_seen_app.update_column(:status, 'seen')
      expect(described_class.not_seen).to be_an_empty
    end
  end
end
