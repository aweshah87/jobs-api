# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Post, type: :model do
  describe '#validations' do
    let(:post) { build(:post) }

    it 'tests that factory is valid' do
      expect(post).to be_valid # post.valid? == true
      post.save!
      another_post = build(:post)
      expect(another_post).to be_valid
    end

    it 'has an invalid title' do
      post.title = ''
      expect(post).not_to be_valid
      expect(post.errors[:title]).to include("can't be blank")
    end

    it 'has an invalid description' do
      post.description = ''
      expect(post).not_to be_valid
      expect(post.errors[:description]).to include("can't be blank")
    end

    it 'has an invalid expiry date' do
      post.expiry_date = ''
      expect(post).not_to be_valid
      expect(post.errors[:expiry_date]).to include("can't be blank")
    end
  end

  describe '.not_expired' do
    it 'returns expired posts' do
      expired_post = create(:post, expiry_date: 1.hour.ago)

      expect(described_class.not_expired).to be_an_empty

      expired_post.update_column(:expiry_date, 2.hours.after)

      expect(described_class.not_expired).to eq(
        [expired_post]
      )
    end
  end
end
