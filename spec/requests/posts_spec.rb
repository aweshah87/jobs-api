# frozen_string_literal: true

require 'rails_helper'
RSpec.describe 'Post', type: :request do
  let(:user) { create(:user)}
  let(:admin) { create(:user, is_admin: 1) }
  before(:each) do
    sign_in user
    @post_params = {
      title: 'job post title',
      description: 'job post description',
      expiry_date: 1.day.after
    }
    @new_post_params = {
      title: 'job post title new',
      description: 'job post description new',
      expiry_date: 1.day.after
    }
  end
  describe 'GET posts#index' do
    it 'should get index' do
      auth_tokens = user.create_new_auth_token
      get '/posts', headers: { "access-token": auth_tokens['access-token'],
                               "client": auth_tokens['client'],
                               "uid": auth_tokens['uid'] }
      expect(response).to have_http_status(200)
    end
  end
  describe 'POST posts#create' do
    it 'user create post with valid attributes' do
      auth_tokens = user.create_new_auth_token
      post '/posts', params: @post_params.to_json, headers: { "Content-Type": 'application/json',
                                                              "access-token": auth_tokens['access-token'],
                                                              "client": auth_tokens['client'],
                                                              "uid": auth_tokens['uid'] }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(403)
    end
    it 'admin create post with valid attributes' do
      auth_tokens = admin.create_new_auth_token
      post '/posts', params: @post_params.to_json, headers: { "Content-Type": 'application/json',
                                                              "access-token": auth_tokens['access-token'],
                                                              "client": auth_tokens['client'],
                                                              "uid": auth_tokens['uid'] }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(201)
    end
  end
  describe 'PUT posts#update' do
    it 'user should update the post' do
      auth_tokens = user.create_new_auth_token
      post = create(:post)
      put "/posts/#{post.id}", params: @new_post_params.to_json, headers: { "Content-Type": 'application/json',
                                                                            "access-token": auth_tokens['access-token'],
                                                                            "client": auth_tokens['client'],
                                                                            "uid": auth_tokens['uid'] }
      expect(response).to have_http_status(403)
    end
    it 'admin should update the post' do
      auth_tokens = admin.create_new_auth_token
      post = create(:post)
      put "/posts/#{post.id}", params: @new_post_params.to_json, headers: { "Content-Type": 'application/json',
                                                                            "access-token": auth_tokens['access-token'],
                                                                            "client": auth_tokens['client'],
                                                                            "uid": auth_tokens['uid'] }
      expect(response).to have_http_status(200)
    end
  end
  describe 'DELETE posts#delete' do
    it 'user should delete the post' do
      auth_tokens = user.create_new_auth_token
      post = create(:post)
      delete "/posts/#{post.id}", headers: { "Content-Type": 'application/json',
                                             "access-token": auth_tokens['access-token'],
                                             "client": auth_tokens['client'],
                                             "uid": auth_tokens['uid'] }
      expect(response).to have_http_status(403)
    end
    it 'admin should delete the post' do
      auth_tokens = admin.create_new_auth_token
      post = create(:post)
      delete "/posts/#{post.id}", headers: { "Content-Type": 'application/json',
                                             "access-token": auth_tokens['access-token'],
                                             "client": auth_tokens['client'],
                                             "uid": auth_tokens['uid'] }
      expect(response).to have_http_status(204)
    end
  end

end
