# frozen_string_literal: true

require 'rails_helper'
RSpec.describe 'Application', type: :request do
  let(:user) {create(:user)}
  before(:each) do
    sign_in user
    post = create(:post)
    @application_params = {
      post_id: post.id
    }
    new_post = create(:post)
    @new_application_params = {
      post_id: new_post.id
    }
  end
  describe 'GET applications#index' do
    it 'should get index' do
      auth_tokens = user.create_new_auth_token
      get '/applications', headers: { "access-token": auth_tokens['access-token'],
                                      "client": auth_tokens['client'],
                                      "uid": auth_tokens['uid'] }
      expect(response).to have_http_status(200)
    end
  end
  describe 'POST applications#create' do
    it 'create application with valid attributes' do
      auth_tokens = user.create_new_auth_token
      post '/applications', params: @application_params.to_json, headers: { "Content-Type": 'application/json',
                                                                            "access-token": auth_tokens['access-token'],
                                                                            "client": auth_tokens['client'],
                                                                            "uid": auth_tokens['uid'] }
      json = JSON.parse(response.body)
      expect(response).to have_http_status(201)
    end
  end
  describe 'PUT applications#update' do
    it 'should update the application' do
      auth_tokens = user.create_new_auth_token
      application = create(:application)
      put "/applications/#{application.id}", params: @new_application_params.to_json, headers: { "Content-Type": 'application/json',
                                                                                                 "access-token": auth_tokens['access-token'],
                                                                                                 "client": auth_tokens['client'],
                                                                                                 "uid": auth_tokens['uid'] }
      expect(response).to have_http_status(200)
    end
  end
  describe 'DELETE applications#delete' do
    it 'should not delete the application' do
      auth_tokens = user.create_new_auth_token
      application = create(:application)
      delete "/applications/#{application.id}", headers: { "Content-Type": 'application/json',
                                                           "access-token": auth_tokens['access-token'],
                                                           "client": auth_tokens['client'],
                                                           "uid": auth_tokens['uid'] }
      expect(response).to have_http_status(403)
    end
  end

end
