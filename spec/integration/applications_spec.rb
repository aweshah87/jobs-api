require 'swagger_helper'

RSpec.describe 'Applications API' do
  path '/applications' do
    post 'Creates a job application' do
      tags 'Job Applications'
      consumes 'application/json'
      parameter name: 'access-token', in: :header, schema: {
        type: :string
      }
      parameter name: 'client', in: :header, schema: {
        type: :string
      }
      parameter name: 'uid', in: :header, schema: {
        type: :string
      }
      parameter name: :job_application, in: :body, schema: {
        type: :object,
        properties: {
          post_id: { type: :integer }
        },
        required: [ 'post_id']
      }

      response '201', 'application created' do
        let(:job_application) { create(:application) }
        run_test!
      end

      response '422', 'invalid request' do
        let(:job_application) { create(:application, post_id: '') }
        run_test!
      end
    end
    get 'get all job applications' do
      tags 'Job Applications'
      consumes 'application/json'
      parameter name: 'access-token', in: :header, schema: {
        type: :string
      }
      parameter name: 'client', in: :header, schema: {
        type: :string
      }
      parameter name: 'uid', in: :header, schema: {
        type: :string
      }
      parameter name: 'page[number]', in: :path, schema: {
        type: :string
      }
      parameter name: 'page[size]', in: :path, schema: {
        type: :string
      }

      response '200', 'list applications' do
        let(:job_application) { create(:application) }
        run_test!
      end

      response '422', 'invalid request' do
        let(:job_post) { create(:application, post_id: '') }
        run_test!
      end
    end
  end

  path '/applications/{id}' do
    get 'Retrieves a application' do
      tags 'Job Applications'
      produces 'application/json'
      parameter name: 'access-token', in: :header, schema: {
        type: :string
      }
      parameter name: 'client', in: :header, schema: {
        type: :string
      }
      parameter name: 'uid', in: :header, schema: {
        type: :string
      }
      parameter name: :id, in: :path, type: :string

      response '200', 'job application found' do
        schema '$ref' => '#/components/schemas/job_application'
        let(:id) { create(:application).id }
        run_test!
      end

      response '404', 'post not found' do
        let(:id) { 'invalid' }
        run_test!
      end
    end
    put 'Update a application' do
      tags 'Job Applications'
      produces 'application/json'
      parameter name: 'access-token', in: :header, schema: {
        type: :string
      }
      parameter name: 'client', in: :header, schema: {
        type: :string
      }
      parameter name: 'uid', in: :header, schema: {
        type: :string
      }
      parameter name: :id, in: :path, type: :string

      response '200', 'job application updated' do
        schema '$ref' => '#/components/schemas/job_application'
        let(:id) { create(:application).id }
        run_test!
      end

      response '404', 'application not found' do
        let(:id) { 'invalid' }
        run_test!
      end
    end
    delete 'Delete a application' do
      tags 'Job Applications'
      produces 'application/json'
      parameter name: 'access-token', in: :header, schema: {
        type: :string
      }
      parameter name: 'client', in: :header, schema: {
        type: :string
      }
      parameter name: 'uid', in: :header, schema: {
        type: :string
      }
      parameter name: :id, in: :path, type: :string

      response '204', 'job application deleted' do
        let(:id) { create(:post).id }
        run_test!
      end

      response '404', 'application not found' do
        let(:id) { 'invalid' }
        run_test!
      end
    end
  end
end