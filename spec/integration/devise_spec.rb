require 'swagger_helper'

RSpec.describe 'DeviseAuthToken API' do
  path '/auth' do
    post 'Sign up user' do
      tags 'User'
      consumes 'application/json'
      parameter name: :user_reg, in: :body, schema: {
        type: :object,
        properties: {
          email: { type: :string },
          password: { type: :string },
          password_confirmation: { type: :string }
        }
      }

      response '201', 'user created' do
        let(:user_reg) { create(:user) }
        run_test!
      end
    end
  end
  path '/auth/sign_in' do
    post 'Sign in a user' do
      tags 'User'
      consumes 'application/json'
      parameter name: :user_reg, in: :body, schema: {
        type: :object,
        properties: {
          email: { type: :string },
          password: { type: :string }
        }
      }

      response '200', 'user found' do
        let(:user_reg) { create(:user) }
        run_test!
      end
    end
  end
end