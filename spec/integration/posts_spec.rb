require 'swagger_helper'

RSpec.describe 'Posts API' do

  path '/posts' do

    post 'Creates a job post' do
      tags 'Job Posts'
      consumes 'application/json'
      parameter name: 'access-token', in: :header, schema: {
        type: :string
      }
      parameter name: 'client', in: :header, schema: {
        type: :string
      }
      parameter name: 'uid', in: :header, schema: {
        type: :string
      }
      parameter name: :job_post, in: :body, schema: {
        type: :object,
        properties: {
          title: { type: :string },
          description: { type: :string },
          expiry_date: { type: :string, format: "date-time" }
        },
        required: [ 'title', 'description', 'expiry_date' ]
      }

      response '201', 'post created' do
        schema '$ref' => '#/components/schemas/job_post'
        let(:job_post) { create(:post) }
        run_test!
      end

      response '422', 'invalid request' do
        schema '$ref' => '#/components/schemas/errors_object'
        let(:job_post) { create(:post, expiry_date: '') }
        run_test!
      end
    end
    get 'get all job posts' do
      tags 'Job Posts'
      consumes 'application/json'
      parameter name: 'access-token', in: :header, schema: {
        type: :string
      }
      parameter name: 'client', in: :header, schema: {
        type: :string
      }
      parameter name: 'uid', in: :header, schema: {
        type: :string
      }
      parameter name: 'page[number]', in: :path, schema: {
        type: :string
      }
      parameter name: 'page[size]', in: :path, schema: {
        type: :string
      }

      response '200', 'list posts' do
        let(:job_post) { create(:post) }
        run_test!
      end

      response '422', 'invalid request' do
        schema '$ref' => '#/components/schemas/errors_object'
        let(:job_post) { create(:post, expiry_date: '') }
        run_test!
      end
    end
  end

  path '/posts/{id}' do

    get 'Retrieves a post' do
      tags 'Job Posts'
      produces 'application/json'
      parameter name: 'access-token', in: :header, schema: {
        type: :string
      }
      parameter name: 'client', in: :header, schema: {
        type: :string
      }
      parameter name: 'uid', in: :header, schema: {
        type: :string
      }
      parameter name: :id, in: :path, type: :string

      response '200', 'job post found' do
        schema '$ref' => '#/components/schemas/job_post'
        let(:id) { create(:post).id }
        run_test!
      end

      response '404', 'post not found' do
        schema '$ref' => '#/components/schemas/errors_object'
        let(:id) { 'invalid' }
        run_test!
      end
    end
    put 'Update a post' do
      tags 'Job Posts'
      produces 'application/json'
      parameter name: 'access-token', in: :header, schema: {
        type: :string
      }
      parameter name: 'client', in: :header, schema: {
        type: :string
      }
      parameter name: 'uid', in: :header, schema: {
        type: :string
      }
      parameter name: :id, in: :path, type: :string

      response '200', 'job post updated' do
        schema '$ref' => '#/components/schemas/job_post'
        let(:id) { create(:post).id }
        run_test!
      end

      response '404', 'post not found' do
        schema '$ref' => '#/components/schemas/errors_object'
        let(:id) { 'invalid' }
        run_test!
      end
    end
    delete 'Delete a post' do
      tags 'Job Posts'
      produces 'application/json'
      parameter name: 'access-token', in: :header, schema: {
        type: :string
      }
      parameter name: 'client', in: :header, schema: {
        type: :string
      }
      parameter name: 'uid', in: :header, schema: {
        type: :string
      }
      parameter name: :id, in: :path, type: :string

      response '204', 'job post deleted' do
        let(:id) { create(:post).id }
        run_test!
      end

      response '404', 'post not found' do
        schema '$ref' => '#/components/schemas/errors_object'
        let(:id) { 'invalid' }
        run_test!
      end
    end
  end
end