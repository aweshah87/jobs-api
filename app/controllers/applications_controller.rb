# frozen_string_literal: true

class ApplicationsController < ApplicationController
  include Paginable
  before_action :authenticate_user!
  load_and_authorize_resource
  def index
    paginated = if current_user.admin?
                  paginate(Application.not_seen)
                else
                  paginate(current_user.applications.not_seen)
                end
    render_collection(paginated)
  end

  def show
    @application.update_column('status', 'seen') if current_user.admin?
    render json: serializer.new(@application)
  end

  def create
    job_application = current_user.applications.create!(application_params)
    render json: serializer.new(job_application), status: :created
  end

  def update
    @application.update(application_params)
    render json: serializer.new(@application), status: :ok
  end

  def destroy
    @application.destroy
    head :no_content
  end

  private

  def application_params
    params.permit(:post_id)
  end

  def serializer
    ApplicationSerializer
  end
end
