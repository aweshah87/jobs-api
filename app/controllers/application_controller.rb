class ApplicationController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken
  before_action :configure_permitted_parameters, if: :devise_controller?
  include JsonapiErrorsHandler
  ErrorMapper.map_errors!({
                            'ActiveRecord::RecordNotFound' =>
                              'JsonapiErrorsHandler::Errors::NotFound',
                            'CanCan::AccessDenied' =>
                              'JsonapiErrorsHandler::Errors::Forbidden'
                          })
  rescue_from ::StandardError, with: lambda { |e| handle_error(e) }
  rescue_from ActiveRecord::RecordInvalid, with: lambda { |e| handle_validation_error(e) }
  rescue_from ActiveModel::ValidationError, with: lambda { |e| handle_validation_error(e) }

  def handle_validation_error(error)
    error_model = error.try(:model) || error.try(:record)
    mapped = JsonapiErrorsHandler::Errors::Invalid.new(errors: {
                                                         error_model.errors.attribute_names => error_model.errors.full_messages
                                                       })
    render_error(mapped)
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :email])
    devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name, :phone, :email, bank_attributes: [:bank_name, :bank_account]])
  end
end
