# frozen_string_literal: true

class PostsController < ApplicationController
  include Paginable
  before_action :authenticate_user!
  load_and_authorize_resource
  def index
    paginated = if current_user.is_admin
                  paginate(current_user.post)
                else
                  paginate(Post.not_expired)
                end
    render_collection(paginated)
  end

  def show
    render json: serializer.new(@post)
  end

  def create
    pp current_user
    post = current_user.post.create!(post_params)
    render json: serializer.new(post), status: :created
  end

  def update
    @post.update(post_params)
    render json: serializer.new(@post), status: :ok
  end

  def destroy
    @post.destroy
    head :no_content
  end

  private

  def post_params
    params.permit(:title, :description, :expiry_date)
  end

  def serializer
    PostSerializer
  end
end
