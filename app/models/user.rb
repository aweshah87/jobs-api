# frozen_string_literal: true

class User < ActiveRecord::Base
  extend Devise::Models
  has_many :applications, dependent: :destroy
  has_many :post, dependent: :destroy
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  include DeviseTokenAuth::Concerns::User

  def admin?
    true if is_admin
  end

end
