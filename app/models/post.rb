class Post < ApplicationRecord
  belongs_to :user
  has_many :post
  validates :title, presence: true
  validates :description, presence: true
  validates :expiry_date, not_in_past: true

  scope :not_expired, -> { where('expiry_date >= :date', { date: Time.now }) }
end
