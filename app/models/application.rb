class Application < ApplicationRecord
  validates_presence_of :post, scope: :not_expired, message: 'Post object must be present and exists'
  belongs_to :user
  belongs_to :post

  scope :not_seen, -> { where('status = :not_seen', { not_seen: 'not_seen' }) }
end
