# frozen_string_literal: true

class ApplicationSerializer
  include JSONAPI::Serializer

  attributes :post_id, :status
end
