# frozen_string_literal: true

class PostSerializer
  include JSONAPI::Serializer

  attributes :title, :description, :expiry_date
end
