# ZenHR Jobs API Task
This is an a submission for Job API task using extra gems from what task described 

* ```jsom-pagination``` for pagination
* ```jsonapi_errors_handler``` for Error handling
* ```jsonapi-serializer``` for serializing output
* ```rswag-*``` to document APIs

## Deploy Locally steps
After Pull the code these steps needed to run app

* Run ```bundle install``` on project Dir
* Add username/password for database in config/database.yml
* Run ```rake db:migrate``` on project Dir
* In Environment Variables add following VARS:
  * HOST_URL
  * ADMIN_EMAIL
  * ADMIN_PASSWORD
  
* Run ```rake db:seed``` on project Dir
* Run ```rspec``` on project Dir (still integration test cases not working with rspec)
* Run ```rails rswag``` on project Dir to generate swagger document
* Navigate to HOST_URL/api-docs to get APIs



### A note why I convert from sqlite3 to postgres

According to this article https://devcenter.heroku.com/articles/sqlite3 Heroku App not supported sqlite3

###Heroku App URL
https://zenhr-task-wshah.herokuapp.com/api-docs/index.html

* Admin Credentials on Heroku App
  * ADMIN_EMAIL: admin@admin.com
  * ADMIN_PASSWORD: 123456
  
### Postman APIs Documentation
* https://documenter.getpostman.com/view/13419572/TzCL8onc